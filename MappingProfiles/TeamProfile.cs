﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using HomeWork1ConsoleAppHttpClient.DTO;
using HomeWork1ConsoleAppHttpClient.Models;

namespace HomeWork1ConsoleAppHttpClient.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<TeamDTO, Team>();
        }
    }
}
