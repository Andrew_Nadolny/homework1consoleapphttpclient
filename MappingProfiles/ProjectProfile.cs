﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using HomeWork1ConsoleAppHttpClient.DTO;
using HomeWork1ConsoleAppHttpClient.Models;

namespace HomeWork1ConsoleAppHttpClient.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectDTO, Project>().ForMember(x => x.Team, s => s.MapFrom(z => new Team { Id = z.TeamId })).ForMember(x => x.Author, s => s.MapFrom(z => new User { Id = z.AuthorId }));
        }
    }
}
