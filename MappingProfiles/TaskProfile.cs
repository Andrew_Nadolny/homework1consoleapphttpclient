﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using HomeWork1ConsoleAppHttpClient.DTO;
using HomeWork1ConsoleAppHttpClient.Models;

namespace HomeWork1ConsoleAppHttpClient.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskDTO, Task>().ForMember(x=> x.Performer, s => s.MapFrom(z => new User { Id = z.PerformerId }));
        }
    }
}
