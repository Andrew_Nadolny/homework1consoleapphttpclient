﻿using System;
using System.Collections.Generic;
using HomeWork1ConsoleAppHttpClient.DTO;
using HomeWork1ConsoleAppHttpClient.Models;
using HomeWork1ConsoleAppHttpClient.Reguests;
using System.Linq;
using AutoMapper;
using HomeWork1ConsoleAppHttpClient.Services;

namespace HomeWork1ConsoleAppHttpClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Task, TaskDTO>());
            var mapper = new Mapper(config);
            var projects = GetProjectsAsync();
            while (true) 
            {
                Console.WriteLine("1. Get the number of tasks from the project of a specific\nuser.\n");
                Console.WriteLine("2. Get a list of tasks assigned to a specific user,\nwhere name is a task < 45 characters(a collection of tasks).\n");
                Console.WriteLine("3. Get the tasks that\n are finished(finished) in the current(2021) year for a \nspecific user(by id).\n");
                Console.WriteLine("4 .Get a list(id, team name and list of users) from\nteams whose members are over 10 years old, sorted by user\nregistration date in descending order,\n and also grouped by teams\n.");
                Console.WriteLine("5. Get a list of users alphabetically sorted(ascending)\nwith tasks sorted by name length(descending).\n");
                Console.WriteLine("6. Get the following structure(pass user Id to parameters):");
                Console.WriteLine("User");
                Console.WriteLine("User's last project (by creation date)");
                Console.WriteLine("Total number of tasks under the last project");
                Console.WriteLine("Total number of unfinished or canceled tasks for the user");
                Console.WriteLine("Longest user task by date(first created - last completed).\n");
                Console.WriteLine("7.Get the following structure:");
                Console.WriteLine("Project");
                Console.WriteLine("The longest task of the project(by description)");
                Console.WriteLine("Shortest project task(by name)");
                Console.WriteLine("The total number of users in the project team, where either\nthe project description > 20 characters or the number of tasks < 3");
                Console.WriteLine("\n\nEnter the menu item number: ");
                int selectedItem = 0;
                if (int.TryParse(Console.ReadLine(), out selectedItem))
                {
                    int selectedUser = 0;
                    if(selectedItem < 4)
                    {
                        Console.WriteLine("Enter user Id:");
                        if(int.TryParse(Console.ReadLine(), out selectedUser))
                        {
                            if (!new UserService().GetUsers().Any(x => x.Id == selectedUser))
                            {
                                Console.WriteLine("User not found.");
                                Console.ReadKey();
                                Console.Clear();
                                continue;
                            }
                        }
                        else
                        {
                            Console.WriteLine("The entered value is not a valid numeric value.");
                            Console.ReadKey();
                            Console.Clear();
                            continue;
                        }
                    }
                    switch (selectedItem)
                    {
                        case 1:
                            var CountTasksInProjectByAuthorId = GetCountTasksInProjectByAuthorId(projects, selectedUser);
                            if (CountTasksInProjectByAuthorId.Count() == 0)
                            {
                                Console.WriteLine("No tasks");
                                break;
                            }
                            foreach (var pair in CountTasksInProjectByAuthorId)
                            {
                                Console.WriteLine("ProjectId: "+pair.Key.Id +" Number of tasks:"+ pair.Value);
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                            }
                            break;
                        case 2:
                            var UserTasksWithShortNameByUserId = GetUserTasksWithShortNameByUserId(projects, selectedUser);
                            if (UserTasksWithShortNameByUserId.Count() == 0)
                            {
                                Console.WriteLine("No tasks");
                                break;
                            }
                            foreach (var task in UserTasksWithShortNameByUserId)
                            {
                                Console.WriteLine(task);
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                            }
                            break;
                        case 3:
                            var UserTasksFinishedInCurrentYear = GetUserTasksFinishedInCurrentYear(projects, selectedUser);
                            if (UserTasksFinishedInCurrentYear.Count() == 0)
                            {
                                Console.WriteLine("No tasks");
                                break;
                            }
                            foreach (var task in UserTasksFinishedInCurrentYear)
                            {
                                Console.WriteLine(task);
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                            }
                            break;
                        case 4:
                            var ListOfTeamsWithTeamatesOlderThen10 = GetListOfTeamsWithTeamatesOlderThen10(projects);
                            if (ListOfTeamsWithTeamatesOlderThen10.Count() == 0)
                            {
                                Console.WriteLine("No teams");
                                break;
                            }
                            foreach (var team in ListOfTeamsWithTeamatesOlderThen10)
                            {
                                string userInfo = "";
                                foreach (var teammate in team.Teammates)
                                {
                                    userInfo += teammate.ToString()+ "\n";
                                }
                                Console.WriteLine(string.Format("\nTeam:\n[\nId:{0},\nName:{1},\nUsers:{2}\n]\n", team.Id, team.Name, userInfo));
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

                            }
                            break;
                        case 5:
                            var ListOfUsersAscendingWhitTasksDescending = GetListOfUsersAscendingWhitTasksDescending(projects);
                            if (ListOfUsersAscendingWhitTasksDescending.Count() == 0)
                            {
                                Console.WriteLine("No results");
                                break;
                            }
                            foreach (var tuple in ListOfUsersAscendingWhitTasksDescending)
                            {
                                string taskInfo = "";
                                foreach (var task in tuple.Tasks)
                                {
                                    taskInfo += task.ToString() + "\n";
                                }
                                Console.WriteLine(string.Format("\nTuple:\n[\nUser:{0},\nTasks:{1}\n]\n", tuple.User, taskInfo));
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

                            }
                            break;
                        case 6:
                            var UserWithLastProjectAndETC = GetUserWithLastProjectAndETC(projects);
                            if (UserWithLastProjectAndETC.Count() == 0)
                            {
                                Console.WriteLine("No results");
                                break;
                            }
                            foreach (var tuple in UserWithLastProjectAndETC)
                            {
                                string taskInfo = "";
                                if(tuple.LastProjectTask != null)
                                {
                                    foreach (var task in tuple.LastProjectTask)
                                    {
                                        taskInfo += task.ToString() + "\n";
                                    }
                                }
                                Console.WriteLine(string.Format("\nTuple:\n[\nUser:{0},\nLast projects:{1},\nLast project tasks:{2},\nCount of finished or canceled tasks:{3},\nLongest task:{4}\n]\n", tuple.User, tuple.LastProject, taskInfo, tuple.CountOfUnfinishedAndCanceledTasks, tuple.LongesUserTask));
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                            }
                            break;
                        case 7:
                            var ProjectWithTaskWithLongestDescriptionsAndETC = GetProjectWithTaskWithLongestDescriptionsAndETC(projects);
                            if (ProjectWithTaskWithLongestDescriptionsAndETC.Count() == 0)
                            {
                                Console.WriteLine("No tasks");
                                break;
                            }
                            foreach (var task in ProjectWithTaskWithLongestDescriptionsAndETC)
                            {
                                Console.WriteLine(task);
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                            }
                            break;
                        default:
                            Console.WriteLine("There are no item with this number.");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("The entered value is not a valid numeric value.");
                }
                Console.WriteLine("Press any key to continue.");
                Console.ReadKey();
                Console.Clear();
            }
        }
        public static List<Project> GetProjectsAsync()
        {
            ProjectService projectService = new ProjectService();
            TaskService taskService = new TaskService();
            UserService userService = new UserService();
            TeamService teamService = new TeamService();

            List<Project> projects = projectService.GetProjects();
            List<User> users = userService.GetUsers();
            List<Team> teams = teamService.GetTeams();
            List<Task> tasks = taskService.GetTasks();
            var taskQuery = from task in tasks
                            join user in users on task.Performer.Id equals user.Id
                            select new Task
                            {
                                TaskState = task.TaskState,
                                Description = task.Description,
                                CreatedAt = task.CreatedAt,
                                FinishedAt = task.FinishedAt,
                                Id = task.Id,
                                Name = task.Name,
                                ProjectId = task.ProjectId,
                                Performer = user
                            };
            var projectsQuery = from project in projects
                                join team in teams on project.Team.Id equals team.Id
                                join task in taskQuery on project.Id equals task.ProjectId into prts
                                join user in users on project.Author.Id equals user.Id
                                select new Project
                                {
                                    Author = user,
                                    CreatedAt = project.CreatedAt,
                                    Deadline = project.Deadline,
                                    Description = project.Description,
                                    Id = project.Id,
                                    Name = project.Name,
                                    Tasks = prts.ToList(),
                                    Team = team
                                };
            return projectsQuery.ToList();
        }

        public static Dictionary<Project, int> GetCountTasksInProjectByAuthorId(List<Project> projects, int userId)
        {
            return projects.Where(x => x.Author.Id == userId).ToDictionary(x => x, x => x.Tasks.Count());
        }

        public static List<Task> GetUserTasksWithShortNameByUserId(List<Project> projects, int userId)
        {
            return projects.SelectMany(x => x.Tasks).Where(x => x.Performer.Id == userId && x.Name.Length < 45).ToList();
        }

        public static List<Task> GetUserTasksFinishedInCurrentYear(List<Project> projects, int userId)
        {
            return projects.SelectMany(x => x.Tasks).Where(x => x.FinishedAt.Year == DateTime.Now.Year && x.Performer.Id == userId).ToList();
        }

        public static List<(int Id, string Name, List<User> Teammates)> GetListOfTeamsWithTeamatesOlderThen10(List<Project> projects)
        {
            return projects.Select(x => x.Team)
                .Distinct()
                .Select(
                 team => (
                 Id: team.Id,
                 Name: team.Name,
                 Teammates: projects.SelectMany(x => x.Tasks.Select(x => x.Performer)).Union(projects.Select(x => x.Author)).Distinct().Where(x => x.TeamId == team.Id).OrderBy(x => x.RegisteredAt).ToList()
            )).Where(x => !x.Teammates.Any(x => x.BirthDay > DateTime.Now.AddYears(-10))).ToList();
        }

        public static List<(User User, List<Task> Tasks)> GetListOfUsersAscendingWhitTasksDescending(List<Project> projects)
        {
            return projects.SelectMany(x => x.Tasks).Select(x => x.Performer).Union(projects.Select(x => x.Author)).Distinct().OrderBy(x => x.FirstName.Length)
                .Select(
                user => (
                    User: user,
                    Tasks: projects.SelectMany(x => x.Tasks).Where(x=> x.Performer.Id == user.Id).OrderByDescending(x => x.Name.Length).ToList()
            )).ToList();
        }

        public static List<(User User, Project LastProject, List<Task> LastProjectTask, int CountOfUnfinishedAndCanceledTasks, Task LongesUserTask)> 
            GetUserWithLastProjectAndETC(List<Project> projects)
        {
            return projects.SelectMany(project => project.Tasks).Select(task => task.Performer).Union(projects.Select(x => x.Author)).Distinct().Select(user => (
                User: user,
                LastProject: projects.Where(project => project.Author.Id == user.Id).OrderBy(project => project.CreatedAt)?.FirstOrDefault(),
                LastProjectTask: projects.Where(project => project.Author.Id == user.Id).OrderBy(project => project.CreatedAt).FirstOrDefault()?.Tasks,
                CountOfUnfinishedAndCanceledTasks: projects.SelectMany(project => project.Tasks).Where(x => x.Performer.Id == user.Id && (x.FinishedAt!= DateTime.MinValue || x.TaskState == TaskState.Canceled )).Count(),
                LongesUserTask : projects.SelectMany(project => project.Tasks).Where(task => task.Performer.Id == user.Id).DefaultIfEmpty().Aggregate((longest, next) =>
                        (next.FinishedAt - next.CreatedAt) > (longest.FinishedAt - longest.CreatedAt) ? next : longest)
            )).ToList();
        }

        public static List<(Project Project, Task TaskWithLongestDescriptions, Task TaskWithShortestName, int CountOfUsersInProjects)>
            GetProjectWithTaskWithLongestDescriptionsAndETC(List<Project> projects)
        {
            return projects.Select(project => (
            Project: project,
            TaskWithLongestDescriptions: project.Tasks.DefaultIfEmpty().Aggregate((longest, next) =>
                        next.Description.Length > longest.Description.Length ? next : longest),
            TaskWithShortestName: project.Tasks.DefaultIfEmpty().Aggregate((shortests, next) =>
                        next.Name.Length < shortests.Name.Length ? next : shortests),
            CountOfUsersInProjects: (project.Description.Length > 20 || project.Tasks.Count() < 3) ? projects.SelectMany(x => x.Tasks).Select(x => x.Performer).Union(projects.Select(x=>x.Author)).Distinct().Where(x => x.TeamId == project.Team.Id).Count() : 0
            )).ToList();
        }
    }
}
