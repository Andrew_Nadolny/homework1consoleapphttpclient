﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork1ConsoleAppHttpClient.Models
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
}
