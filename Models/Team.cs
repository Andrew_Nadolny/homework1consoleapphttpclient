﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork1ConsoleAppHttpClient.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public override string ToString()
        {
            return string.Format("\nTeam: [ Id:{0}, Name:{1}, CreatedAt:{2} ]\n", Id, Name, CreatedAt);
        }
    }
}
