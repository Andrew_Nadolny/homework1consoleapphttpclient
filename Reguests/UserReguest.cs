﻿using HomeWork1ConsoleAppHttpClient.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork1ConsoleAppHttpClient.Reguests
{
    class UserRequest
    {
        public string ApiUrl = "https://bsa21.azurewebsites.net";
        public async Task<List<UserDTO>> GetUsersAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                List<UserDTO> usersDTOs = new List<UserDTO>();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync("/api/Users");
                if (response.IsSuccessStatusCode)
                {
                    usersDTOs = JsonConvert.DeserializeObject<List<UserDTO>>(await response.Content.ReadAsStringAsync());
                }
                return usersDTOs;
            }
        }

        public async Task<UserDTO> GetUserAsync(int userId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                UserDTO userDTO = new UserDTO();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/Users/{0}", userId));
                if (response.IsSuccessStatusCode)
                {
                    userDTO = JsonConvert.DeserializeObject<UserDTO>(await response.Content.ReadAsStringAsync());
                }
                return userDTO;
            }
        }
    }
}
