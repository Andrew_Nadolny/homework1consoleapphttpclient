﻿using HomeWork1ConsoleAppHttpClient.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork1ConsoleAppHttpClient.Reguests
{
    public class TaskRequest
    {
        private string ApiUrl = "https://bsa21.azurewebsites.net";
        public async Task<List<TaskDTO>> GetTasksAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                List<TaskDTO> taskDTOs = new List<TaskDTO>();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync("/api/Tasks");
                if (response.IsSuccessStatusCode)
                {
                    taskDTOs = JsonConvert.DeserializeObject<List<TaskDTO>>(await response.Content.ReadAsStringAsync());
                }
                return taskDTOs;
            }
        }

        public async Task<TaskDTO> GetTaskAsync(int taskId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                TaskDTO tastDTO = new TaskDTO();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/Tasks/{0}",taskId));
                if (response.IsSuccessStatusCode)
                {
                    tastDTO = JsonConvert.DeserializeObject<TaskDTO>(await response.Content.ReadAsStringAsync());
                }
                return tastDTO;
            }
        }
    }
}
