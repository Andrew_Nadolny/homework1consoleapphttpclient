﻿using HomeWork1ConsoleAppHttpClient.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork1ConsoleAppHttpClient.Reguests
{
    class TeamRequest
    {
        public string ApiUrl = "https://bsa21.azurewebsites.net";
        public async Task<List<TeamDTO>> GetTeamsAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                List<TeamDTO> teamDTOs = new List<TeamDTO>();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync("/api/Teams");
                if (response.IsSuccessStatusCode)
                {
                    teamDTOs = JsonConvert.DeserializeObject<List<TeamDTO>>(await response.Content.ReadAsStringAsync());
                }
                return teamDTOs;
            }
        }

        public async Task<TeamDTO> GetTeamAsync(int teamId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                TeamDTO teamsDTO = new TeamDTO();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/Teams/{0}", teamId));
                if (response.IsSuccessStatusCode)
                {
                    teamsDTO = JsonConvert.DeserializeObject<TeamDTO>(await response.Content.ReadAsStringAsync());
                }
                return teamsDTO;
            }
        }
    }
}
