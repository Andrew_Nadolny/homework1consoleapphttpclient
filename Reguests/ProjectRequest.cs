﻿using HomeWork1ConsoleAppHttpClient.DTO;
using HomeWork1ConsoleAppHttpClient.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HomeWork1ConsoleAppHttpClient.Reguests
{
    public class ProjectRequest
    {
        public string ApiUrl = "https://bsa21.azurewebsites.net";
        public async Task<List<ProjectDTO>> GetProjectsAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                List<ProjectDTO> projectDTOs = new List<ProjectDTO>();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync("/api/Projects");
                if (response.IsSuccessStatusCode)
                {
                    projectDTOs = JsonConvert.DeserializeObject<List<ProjectDTO>>(await response.Content.ReadAsStringAsync());
                }
                return projectDTOs;
            }
        }
        public async Task<ProjectDTO> GetProjectAsync(int projectId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                ProjectDTO projectDTO = new ProjectDTO();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/Projects/{0}", projectId));
                if (response.IsSuccessStatusCode)
                {
                    projectDTO = JsonConvert.DeserializeObject<ProjectDTO>(await response.Content.ReadAsStringAsync());
                }
                return projectDTO;
            }
        }
    }
}
