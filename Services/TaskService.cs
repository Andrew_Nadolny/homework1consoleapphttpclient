﻿using HomeWork1ConsoleAppHttpClient.Reguests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using AutoMapper;
using HomeWork1ConsoleAppHttpClient.DTO;
using HomeWork1ConsoleAppHttpClient.Models;
using HomeWork1ConsoleAppHttpClient.MappingProfiles;

namespace HomeWork1ConsoleAppHttpClient.Services
{
    public class TaskService
    {
        private TaskRequest taskRerquest = new TaskRequest();
        Mapper _mapper;

        public TaskService()
        {
            _mapper = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<TaskProfile>()));
        }
        public List<Task> GetTasks()
        {
           return _mapper.Map<List<TaskDTO>, List<Task>>(taskRerquest.GetTasksAsync().Result);
        }

        public Task GetTask(int taskId)
        {
            return _mapper.Map<TaskDTO,Task>(taskRerquest.GetTaskAsync(taskId).Result);
        }
    }
}
