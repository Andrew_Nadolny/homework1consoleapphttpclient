﻿using AutoMapper;
using HomeWork1ConsoleAppHttpClient.DTO;
using HomeWork1ConsoleAppHttpClient.MappingProfiles;
using HomeWork1ConsoleAppHttpClient.Models;
using HomeWork1ConsoleAppHttpClient.Reguests;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork1ConsoleAppHttpClient.Services
{
    public class TeamService
    {
        private TeamRequest teamRequest = new TeamRequest();
        private Mapper _mapper;

        public TeamService()
        {
            _mapper = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<TeamProfile>()));
        }

        public List<Team> GetTeams()
        {
            return _mapper.Map<List<TeamDTO>, List<Team>>(teamRequest.GetTeamsAsync().Result);
        }

        public Team GetTeam(int teamId)
        {
            return _mapper.Map<TeamDTO,Team>(teamRequest.GetTeamAsync(teamId).Result);
        }
    }
}
