﻿using AutoMapper;
using HomeWork1ConsoleAppHttpClient.DTO;
using HomeWork1ConsoleAppHttpClient.MappingProfiles;
using HomeWork1ConsoleAppHttpClient.Models;
using HomeWork1ConsoleAppHttpClient.Reguests;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace HomeWork1ConsoleAppHttpClient.Services
{
    class ProjectService
    {
        private ProjectRequest projectRerquest = new ProjectRequest();
        private Mapper _mapper;

        public ProjectService()
        {
            _mapper  = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<ProjectProfile>()));
        }
        public List<Project> GetProjects()
        {
            return _mapper.Map<List<ProjectDTO>, List<Project>>(projectRerquest.GetProjectsAsync().Result);
        }

        public Project GetProject(int projectId)
        {
            return _mapper.Map<ProjectDTO, Project>(projectRerquest.GetProjectAsync(projectId).Result);
        }
    }
}
