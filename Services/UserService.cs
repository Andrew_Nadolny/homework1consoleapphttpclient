﻿using AutoMapper;
using HomeWork1ConsoleAppHttpClient.DTO;
using HomeWork1ConsoleAppHttpClient.MappingProfiles;
using HomeWork1ConsoleAppHttpClient.Models;
using HomeWork1ConsoleAppHttpClient.Reguests;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork1ConsoleAppHttpClient.Services
{
    public class UserService
    {
        private UserRequest userRequest = new UserRequest();
        private Mapper _mapper;


        public UserService()
        {
            _mapper = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<UserProfile>()));
        }

        public List<User> GetUsers()
        {
            return _mapper.Map<List<User>>(userRequest.GetUsersAsync().Result);
        }

        public User GetUser(int userId)
        {
            return _mapper.Map<UserDTO,User>(userRequest.GetUserAsync(userId).Result);
        }
    }
}
